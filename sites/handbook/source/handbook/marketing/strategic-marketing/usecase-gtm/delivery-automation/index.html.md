---
layout: markdown_page
title: "DevOps Solution Resource: Delivery Automation"
---

## Looking for a customer-facing overview of GitLab's Delivery Automation Capabilities?

See [Continuous Integration](https://about.gitlab.com/solutions/compliance), [Continuous Delivery](https://about.gitlab.com/stages-devops-lifecycle/continuous-delivery/) and [Source Code Management](https://about.gitlab.com/solutions/benefits-of-using-version-control/)

The page below is intended to align GitLab sales and marketing efforts with a single source of truth for our go-to-market efforts around Delivery Automation.

### Who to contact

|     Product Marketing    |    Technical Marketing    |
| ------------------------ | ------------------------- |
| Saumya Upadhyaya (@supadhyaya)  | Itzik Gan Baruch (@itzik ) |


## Background (2 min read)

**Delivery Automation** allows organizations to automate manual, repetitive tasks from their SDLC to improve the overall velocity of the software factory and increase collaboration across dev and ops. This includes both application and infrastructure automation.
- From a GitLab capabilities point of view - it includes SCM, CI, CD, GitOps and AutoDevOps

Delivery Automation allows Elite performers to (as per [State of DevOps 2021](https://cloud.google.com/blog/products/devops-sre/announcing-dora-2021-accelerate-state-of-devops-report) report)
- Deploy on demand (multiple deploys per day)
- Take less than an hour from code commit to deploy
- Take less than an hour to restore services
- Have less than 15% change failure rate (changes that lead to service degradations)

The opportunity is huge. As per the [GitLab DevSecOps survey 2021](https://about.gitlab.com/developer-survey/) -
- Only 25% have achieved test automation
- Not enough code review is done
- Only 19% of ops teams have achieved full automation

Cloud transformation and application modernization is a key CIO initiative for 2022
- Delivery automation becomes key to support the increase in cloud instances (microservices and ephemeral instances mean manually setting up, configuring and tearing down these instances is not feasible anymore)

## Getting Started {#getting-started}

### Who to meet / Ideal Customer Profile {#who-to-meet}
{: .alert .alert-gitlab-orange}

The persona interested in application automation is generally different from the persona interested in infrastructure automation and continuous delivery. Application automation interest comes from development or engineering organizations while infrastructure automation and continuous delivery interest comes from the operations, systems, infrastructure, platform and cloud side of the organization.

| | Enterprise | Mid Market | SMB |
| - | ----------- | -------------- |
| **User Persona** | SCM/CI-> Developer, team lead, DevOps Engineer <br> CD-> DevOps engineer, app operator, sys admin, release manager, platform engineer | SCM/CI-> Developer, team lead, DevOps Engineer <br> CD-> DevOps engineer, app operator, sys admin, release manager, platform engineer | varies |
| **Buyer Persona** | CIO / CTO at C level <br> SCM/CI -> Mgr / Director - App Dev <br> CD -> Mgr / Director - Infra/Ops | CIO / CTO at C level <br> SCM/CI -> Mgr / Director - App Dev <br> CD -> Mgr / Director - Infra/Ops | varies |
| **Tech Stack** | Includes GitHub, Perforce, Jenkins, BitBucket, Subversion, ArgoCD, Harness | Includes GitHub, Perforce, Jenkins, BitBucket, Bamboo, Subversion, ArgoCD, Harness OR the absence of a tech stack OR freemium version of GitLab / GitHub | Absence of tech stack OR freemium version of GitLab / GitHub |
| **Infra / Cloud Provider / Deploy Targets** | AWS or GCP or internal data center or Kubernetes | AWS or GCP (or none) or Kubernetes | AWS or GCP (or none) or Kubernetes |
| **Additional Criteria** | - Automation a key initiative <br> - merger & acquisition <br> - consolidation of CI-CD solutions <br> - increased hiring and projects <br> - hiring new CIO, App Dev, Infra teams | - Automation a key initiative <br> - increased hiring and projects <br> - hiring new CIO, App Dev, Infra teams | - increased hiring and projects <br> - adoption of new cloud/technologies |
| **Business Driver** | - Digital Transformation <br> - Cloud Transformation <br> - Application Modernization <br> - Delivery Automation | - Cloud Transformation <br> - Application Modernization <br> - Delivery Automation | - Cloud Transformation <br> - Application Modernization <br> - Delivery Automation |



### Keywords to listen for  {#keywords}
{: .alert .alert-gitlab-orange}

- Manual efforts
  - including Manual builds, Manual code reviews, Manual peer reviews, Manual testing, manual deployments, manual infrastructure configurations, amongst others.
- Employee dissatisfaction
  - listen to Repetitive tasks, too much time in managing pipelines, setting up and configuring pipelines, setting up and configuring infrastructure, managing releases
- Increase speed / velocity
- Cloud native applications
- Kubernetes and containers
- Multi platform / multi language support
- Cloud transformation
- Application Modernization

### Value discovery {#value-discovery}
{: .alert .alert-gitlab-orange}

### Discovery Questions — Current State: Where’s the pain?

1. **How much of your team’s job is manual?** This should help scope interest. Many teams struggle with little or no automation - resulting in inefficiencies, errors and employee dissatisfaction.  Key areas of manual effort include - unit testing, performance testing, code review, peer reviews, license checks, manual transfer of application images, manual deploy, manual post deploy testing, amongst others.
1. **How much time is your team spending in configuring, fixing, or maintaining your pipelines compared to doing “real work”?** Managing complex pipelines is a real problem. We find that our customers with complicated toolchains have more difficulty managing complex pipelines and supporting integrations. This usually makes implementing and maintaining CI/CD more expensive than originally planned.
1. **Are you able to manage both current and latest technologies with your existing toolset?** Moving to cloud or cloud native technologies like Kubernetes requires a different way of operation - their existing toolset may not be built ground up for such transformations and could require hacking a number of fragile integrations together to support.
1. **Are you able to measure key success metrics for your automation?** Metrics such as Change Lead Time, Deployment Frequency, Mean Time to Resolution, and Change Failure Rate. This will reveal the lack of integration and traceability across various tools being put together to achieve automation.
1. **Are you able to deploy with confidence?** Are you able to have full visibility into the pipeline from idea to production? Are you able to setup your infra, deploy in phases, test, roll back on error - all from a single application?

### Discovery Questions — Future State: Where Would You Like to Be?

1. **What does automation success look like?** What parts of your SDLC would you have fully automated? This can help uncover areas that they immediately see a pain and can help you identify areas of growth for the customer
1. **What would your team do with the time reclaimed from maintaining pipelines?** This provides an opportunity to discuss what true success would look and how freeing resources improves productive value added by the team
1. **What technologies do you see being supported by your CI-CD solution?** This can help uncover future technology adoption and drivers for change. They may also have a need to have various technologies being supported by the same solution
1. **What success metrics would you want to showcase?** What are the short term metrics and long term metrics? This can help uncover areas of success we can immediately showcase
1. **How important are safe, progressive deployments to your organization?** This uncovers the importance of downtime and how flexible deployment options are key to their automation success.



### Common Pains Points  {#common-pains}

| Challenges "before scenarios" | So What? "negative consequences" |
| ----------------------------- | -------------------------------- |
| Manual processes, lack of standardization | Highly inefficient and error prone processes |
| Employee dissatisfaction due to manual and repetitive processes | Employee churn, inability to hire & retain talent |
| Fragile, complex pipelines are tough to maintain | Requires expert, expensive team members who become a single point of failure |
| Adoption of new technologies not fast enough  | Requires new tools, expertise for managing new technologies  |
| Infrastructure configuration not fast enough or repeatable  | Lack of standardization and automation leads to higher downtime and errors  |
| Lack of scalable review and authorization processes  | Highly skilled workers doing menial tasks - leading to dissatisfaction  |
| Lack of safe deployment options and traceability  |  Longer time to repair, downtime and customer perception issues |


### Common benefits  {#common-benefits}

| Desired Future State (“After Scenarios”) | So What? (“Positive Business Outcomes”) |
| ----------------------------- | -------------------------------- |
| Faster time to value with automation and standardization of testing, review, packaging, release and deployment  |  Automation to focus on “real work”, less risk, improved employee satisfaction  |
| Faster mean time to repair with better traceability and deployment options  |  Reduce customer perception issues, downtime, improve retention  |
| Faster adoption of cloud native technologies  | Cloud native first development along with current deployment targets for more flexibility using a single tool   |
| Better employee satisfaction  |  Improved automation, removal of manual / repetitive tasks and reduce integration work improves employee satisfaction  |

### Required capabilities  {#required-capabilities}


| Required capability	 | Customer Metrics |
| ----------------------------- | -------------------------------- |
| Test automation, pipeline configuration management  | Automation to focus on “real work”, less risk, improved employee satisfaction  |
| Change control and collaboration  | Reduce customer perception issues, downtime, improve retention  |
| Safe deployment and roll back  | Cloud native first development along with current deployment targets for more flexibility using a single tool  |
| Code quality, peer reviews  | Improved automation, removal of manual / repetitive tasks and reduce integration work improves employee satisfaction  |

## Positioning value  {#positioning-value}
{: .alert .alert-gitlab-orange}


### Elevator pitch

TBD

### Value Proposition (How GitLab does it?)

TBD

### Differentiators (How GitLab does Delivery Automation better?)

Apart from the single application, DevOps Platform narrative, there are a number of key areas which GitLab does well and better than competitors.

| Differentiator | Description |
| -------------- | ----------- |
| **Multi-platform** | Execute builds on Unix, Windows, OSX, and any other platform that supports Go |
| **Multi-language** | Build scripts work with Java, PHP, Ruby, C, and any other language |
| **Multi-deployment targets** | Deploy to embedded systems, on-premise servers, Raspberry Pi devices, mainframes, virtual machines, Kubernetes clusters, FaaS offerings, multiple clouds like AWS, GCP, Azure, IBM and Oracle |
| **Faster builds** |  GitLab splits builds over multiple machines for parallel execution |
| **Autoscaling** | Automatically spin up and down VM's or Kubernetes pods to make sure your builds get processed immediately while minimizing costs |
| **Versioned tests** | a .gitlab-ci.yml file that contains your tests, allowing developers to contribute changes and ensuring every branch gets the tests it needs |
| **Flexible Pipelines** | Define multiple jobs per stage and even trigger other pipelines |
| **Ease test environment creation** | With the review app - use custom Docker images, spin up services as part of testing, build new Docker images, run on Kubernetes - use this to run UX tests, DAST, usability tests etc |
| **Built in Container Registry** | built-in container registry to store, share, and use container images |
| **Distributed version control** | Reduced time to clone and fetch large repos for geographically distributed teams |
| **Product development management** | Version control not just source code but also IP, graphic assets, animations and binaries to mention a few |
| **Unified deployment and observability** | Visualise what goes into production (Review apps and release planning), what to deploy to production (feature flags) , who to deploy it to (Progressive Delivery and deployment strategies like Canary), monitor performance of deployment (via browser performance testing, performance monitoring/tracing) and rollback based on performance via post deployment monitoring, all from a single application |
| **Secure Kubernetes Cluster access** | Avoid exposing your cluster by establishing a secure connection with your Kubernetes cluster for your CI/CD jobs |
| **Push and pull based deployments** | Majority of customers still prefer the control that push based deployment gives, we support both |


### Competitors

TBD

### Handling Objections

| Objection | Response |
| ----------------------------- | -------------------------------- |
| *“I don’t need another tool to replace my existing tool”*  | - We can meet you where you are - you don’t need to rip and replace all your existing tools <br> - You can start by supplementing GitLab CI or CD or SCM with your existing toolset of Jira, GitHub, Jenkins etc <br> - Once you find value in the specific area (e.g., SCM, CI, CD, Security, Compliance) - you can expand your use of GitLab to achieve better ROI  |
| *“I don’t want to be locked into one company for my entire DevOps lifecycle”*  | - Toolchain sprawl is a reality - many customers have stopped benefitting from DevOps due to this <br> - Application & resource integration & management actually hinders how fast the organization can move. <br> - Customers partner with GitLab to accelerate time to market for new products/services, improve efficiencies & reduce security & compliance risks. Walk me through how your priorities align with these outcomes.  |
| *“Each development team decides what they need”*  | - Allowing each team to decide creates silos of innovation & development. It also significantly limits the impact of your digital transformation. <br> - Each team then also needs to setup, integrate and maintain these integrations. <br> - Describe how this works for you. How many tools have you spent money on as a result?   |




## Resources {#resources}
{: .alert .alert-gitlab-orange}

### Webinars, e-books, whitepapers, videos  {#resources-list}
- TBD

### Customer Stories  {#customer-stories}

| Company | Problem | Solution | Result |
| [Goldman Sachs](https://about.gitlab.com/customers/goldman-sachs/) | Needed to increase developer efficiency & software quality | GitLab Premium (CI/CD, SCM) | Improved 2 builds/day to 1000+/day; simplified workflow & administration |
| [Sopra Steria](https://about.gitlab.com/customers/sopra_steria/) | Needed to standardize a tool for DevOps with single authorization | GitLab Premium (CI/CD, SCM) | Less than 6 minute builds, deploying to AWS, GCP, Azure  |
| [Wag!](https://about.gitlab.com/2019/01/16/wag-labs-blog-post/) | Slow release process taking over 40 minutes | GitLab Ultimate (CI, CD) | Release process reduced to 6 minutes; 8 releases per day with built-in security; a full deployment pipeline to Amazon Elastic Container Service (ECS)  |
| [ANWB](https://about.gitlab.com/customers/anwb/) | Prolonged outages, long time to debug toolset integrations | GitLab Premium (CI, CD) |  Setup for cloud transformation - Google Cloud on K8S |
| [Ticketmaster](https://about.gitlab.com/blog/2017/06/07/continuous-integration-ticketmaster/) | Slow build process prevented innovation | GitLab Premium (CI) | 15x faster - from over 2 hours to 8 minute builds; faster releases and better customer experiences (5 star app reviews)  |
| [Hotjar](https://about.gitlab.com/customers/hotjar/) | Legacy systems, maintenance of tool integration, slow adoption of cloud technologies | GitLab Premium (Ci, CD) |  50% faster deployments, 30% faster build times, 2-15 deploys per day |
| [Paessler](https://about.gitlab.com/customers/paessler-prtg/)  | Large complex pipelines, slow feedback loop | GitLab Premium (Ci, CD) | 75% faster build times, testing from 45 min to 15 min  |

More case studies here -> Filter by Usecase *CI* or *CD* or *SCM* or *GitOps* to find the latest case studies in the [case study board](https://gitlab.com/gitlab-com/marketing/strategic-marketing/customer-reference-content/case-study-content/-/boards/1804878)


### Services {#services}

GitLab Professional Services help customers become efficient in GitLab quickly. GitLab (or a GitLab partner) offers a number of service offerings to support customers.

For the GitOps sales play, consider asking your customer about their workforce's proficiency with git, GitLab, and GitLab CI as they are foundational elements to GitOps. If they are not strong in all of these topics, consider positioning [GitLab with git Basics training](https://about.gitlab.com/services/education/gitlab-basics/) and/or [GitLab CI/CD training](https://about.gitlab.com/services/education/gitlab-ci/).

Advisory/Consulting services to help with GitOps rollout are planned to be rolled out later this year. Please register your interest for a GitOps Advisory offering [here](https://gitlab.com/gitlab-com/customer-success/professional-services-group/ps-leadership-team/ps-practice-management/-/issues/74) to help PS effectively prioritize!

As you position services, you can use our [Services Pitch Deck](https://docs.google.com/presentation/d/1CFR8_ZyE9r4Dk_mjoWGe4ZkhtBimSdN0pylIPu-NAeU/edit#slide=id.g2823c3f9ca_0_9) to help establsh the value of engaging with PS. Other services can be found in our [Complete list of professional service offerings](https://about.gitlab.com/services/)

Talk with @em in the [professional services slack channel](/handbook/customer-success/professional-services-engineering/working-with/#slack) to learn more.
